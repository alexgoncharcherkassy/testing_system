package db.migration;

import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class V20180302094320__insert_admin_user implements JdbcMigration {
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public void migrate(Connection connection) throws Exception {
        String sql = "INSERT INTO public.user (username, first_name, last_name, password, role) VALUES(?, ?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "admin");
            statement.setString(2, "super");
            statement.setString(3, "mario");
            statement.setString(4, passwordEncoder.encode("admin"));
            statement.setString(5, "ROLE_ADMIN");

            statement.execute();
        }
    }
}
