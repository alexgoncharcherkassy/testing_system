package com.geekhub.testing_system.registration;

import com.geekhub.testing_system.user.User;
import com.geekhub.testing_system.exception.UsernameExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegistrationController {
    private final RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping("/registration")
    public ModelAndView registerPage() {
        return new ModelAndView("registrationForm", "registration", new Registration());
    }

    @PostMapping("/registration")
    public ModelAndView register(@Valid Registration registration, BindingResult result, WebRequest request, Errors errors) {
        if (result.hasErrors()) {
            return new ModelAndView("registrationForm", "registration", registration);
        }

        User userAccount = createUserAccount(registration);

        if (userAccount == null) {
            result.rejectValue("username", "message.regError", "Username already taken");
        }

        if (result.hasErrors()) {
            return new ModelAndView("registrationForm", "registration", registration);
        } else {
            return new ModelAndView("redirect:/login?success");
        }
    }

    private User createUserAccount(Registration registration) {
        User registered = null;
        try {
            registered = registrationService.register(registration);
        } catch (UsernameExistException e) {
            return null;
        }

        return registered;
    }
}
