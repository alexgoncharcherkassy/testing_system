package com.geekhub.testing_system.registration;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Registration registration = (Registration) value;

        return registration.getPassword().equals(registration.getMatchPassword());
    }

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {

    }
}
