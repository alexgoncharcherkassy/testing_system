package com.geekhub.testing_system.registration;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@PasswordMatches
public class Registration {
    @NotNull
    @NotBlank(message = "Username could not be empty")
    @Length(min = 5, max = 50, message = "Username could not be less than 5 characters and greater than 50 characters")
    private String username;

    @NotNull
    @NotBlank(message = "First name could not be empty")
    @Length(min = 1, max = 50, message = "First name could not be less than 1 characters and greater than 50 characters")
    private String firstName;

    @NotNull
    @NotBlank(message = "Last name could not be empty")
    @Length(min = 1, max = 50, message = "Last name could not be less than 1 characters and greater than 50 characters")
    private String lastName;

    @NotNull
    @NotBlank(message = "Password could not be empty")
    @Length(min = 5, max = 25, message = "Password could not be less than 5 characters and greater than 25 characters")
    private String password;

    private String matchPassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchPassword() {
        return matchPassword;
    }

    public void setMatchPassword(String matchPassword) {
        this.matchPassword = matchPassword;
    }
}
