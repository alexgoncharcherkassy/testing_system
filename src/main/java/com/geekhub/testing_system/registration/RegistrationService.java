package com.geekhub.testing_system.registration;

import com.geekhub.testing_system.user.User;
import com.geekhub.testing_system.exception.UsernameExistException;

public interface RegistrationService {
    User register(Registration registration) throws UsernameExistException;
}
