package com.geekhub.testing_system.registration;

import com.geekhub.testing_system.user.User;
import com.geekhub.testing_system.user.UserRepository;
import com.geekhub.testing_system.exception.UsernameExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User register(Registration registration) throws UsernameExistException {
        if (usernameExist(registration.getUsername())) {
            throw new UsernameExistException("There is an user with that username: " + registration.getUsername());
        }

        User user = new User();
        user.setRole("ROLE_USER");
        user.setUsername(registration.getUsername());
        user.setFirstName(registration.getFirstName());
        user.setLastName(registration.getLastName());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));

        userRepository.save(user);

        return user;
    }

    private boolean usernameExist(String username) {
        Optional<User> user = userRepository.findByUsername(username);

        return user.isPresent();
    }
}
