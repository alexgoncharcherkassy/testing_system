package com.geekhub.testing_system.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Optional<User> findByUsername(String username) {
        String sql = "SELECT id, username, first_name, last_name, password, role, created_at FROM public.user WHERE username = ?;";
        try {
            User user = jdbcTemplate.queryForObject(sql, new Object[]{username}, new UserRowMapper());

            return Optional.of(user);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public User save(User user) {
        String sql = "INSERT INTO public.user (username, first_name, last_name, password, role, created_at) VALUES (?, ?, ?, ?, ?, ?);";

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(c -> {
            PreparedStatement preparedStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getFirstName());
            preparedStatement.setString(3, user.getLastName());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getRole());
            preparedStatement.setTimestamp(6, Timestamp.valueOf(user.getCreatedAt()));

            return preparedStatement;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();

        for (Map.Entry<String, Object> entry : keys.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("id")) {
                user.setId((Integer) entry.getValue());
            }
        }

        return user;
    }

    @Override
    public List<User> findAllUsersByRole(String role) {
        String sql = "SELECT id, username, first_name, last_name, password, role, created_at FROM public.user WHERE role = ? ORDER BY created_at DESC;";

        return jdbcTemplate.query(sql, new Object[]{role}, new UserRowMapper());
    }

    @Override
    public Optional<User> findById(int id) {
        String sql = "SELECT id, username, first_name, last_name, password, role, created_at FROM public.user WHERE id = ?;";
        try {
            User user = jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserRowMapper());

            return Optional.of(user);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }
}
