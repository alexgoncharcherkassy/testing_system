package com.geekhub.testing_system.user;

import com.geekhub.testing_system.admin.module.Module;
import com.geekhub.testing_system.admin.usermodule.UserModule;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class User {
    private Integer id;
    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private String role;
    private LocalDateTime createdAt;
    private Collection<UserModule> userModules = new ArrayList<>();

    public User() {
        this.setCreatedAt(LocalDateTime.now());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Collection<UserModule> getUserModules() {
        return new ArrayList<>(userModules);
    }

    public void setUserModules(Collection<UserModule> userModules) {
        this.userModules = userModules;
    }

    public boolean containsModule(Module module) {
        return userModules.stream()
                .anyMatch(um -> um.getModule().equals(module));

    }
}
