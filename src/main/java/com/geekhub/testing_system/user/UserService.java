package com.geekhub.testing_system.user;

import java.util.Optional;

public interface UserService {
    Optional<User> findByUsername(String username);

    Optional<User> findById(int id);
}
