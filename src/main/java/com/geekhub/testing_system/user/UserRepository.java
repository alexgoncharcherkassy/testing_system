package com.geekhub.testing_system.user;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    Optional<User> findByUsername(String username);

    User save(User user);

    List<User> findAllUsersByRole(String role);

    Optional<User> findById(int id);
}
