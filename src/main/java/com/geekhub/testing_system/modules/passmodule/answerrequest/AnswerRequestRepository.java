package com.geekhub.testing_system.modules.passmodule.answerrequest;

import com.geekhub.testing_system.modules.passmodule.PassModule;

import java.util.List;

public interface AnswerRequestRepository {
    AnswerRequest create(AnswerRequest answerRequest);

    AnswerRequest edit(AnswerRequest answerRequest);

    AnswerRequest findByPassModuleIdAndAnswerId(int passModuleId, int answerId);

    List<AnswerRequest> findAllAnswerRequestByPassModule(PassModule passModule);
}
