package com.geekhub.testing_system.modules.passmodule.answerrequest;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AnswerRequestRowMapper implements RowMapper<AnswerRequest> {
    @Override
    public AnswerRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
        AnswerRequest answerRequest = new AnswerRequest();
        answerRequest.setId(rs.getInt("id"));
        answerRequest.setPassModuleId(rs.getInt("pass_module_id"));
        answerRequest.setAnswerId(rs.getInt("answer_id"));
        answerRequest.setResult(rs.getBoolean("result"));

        return answerRequest;
    }
}
