package com.geekhub.testing_system.modules.passmodule.answerrequest;

import com.geekhub.testing_system.exception.ResourceNotFoundException;
import com.geekhub.testing_system.exception.ResourceValidationException;
import com.geekhub.testing_system.modules.passmodule.PassModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/modules/passModules/{id}")
public class AnswerRequestController {
    private final AnswerRequestService answerRequestService;

    @Autowired
    public AnswerRequestController(AnswerRequestService answerRequestService) {
        this.answerRequestService = answerRequestService;
    }

    @PostMapping("/answers/{answerId}")
    public AnswerRequest createAnswerRequest(AnswerRequest answerRequest) {
        AnswerRequest createdAnswerRequest = answerRequestService.save(answerRequest);

        if (answerRequest.getId() == null) {
            throw new ResourceValidationException("Answer request", "id", answerRequest.getId());
        }

        return createdAnswerRequest;
    }

    @PostMapping("")
    public PassModule completeModule(@PathVariable Integer id) {
        PassModule passModule = answerRequestService.complete(id);

        if (passModule.getTimeEnd() == null) {
            throw new ResourceNotFoundException("Pass module", "id", id);
        }

        return passModule;
    }
}
