package com.geekhub.testing_system.modules.passmodule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@RequestMapping("/modules/{id}/passModules")
public class PassModuleController {
    private final PassModuleService passModuleService;

    @Autowired
    public PassModuleController(PassModuleService passModuleService) {
        this.passModuleService = passModuleService;
    }

    @PostMapping("")
    public ModelAndView createPassModule(@PathVariable Integer id) {
        Optional<PassModule> inProgressPassModuleByUserModule = passModuleService.findInProgressPassModuleByUserModule(id);

        if (inProgressPassModuleByUserModule.isPresent()) {
            return new ModelAndView("redirect:/modules/" + id + "/passModules/" + inProgressPassModuleByUserModule.get().getId());
        }

        PassModule passModule = passModuleService.create(id);

        return new ModelAndView("redirect:/modules/" + id + "/passModules/" + passModule.getId());
    }

    @GetMapping("/{idPassModule}")
    public ModelAndView process(@PathVariable("idPassModule") Integer idPassModule) {
        Optional<PassModule> passModule = passModuleService.findById(idPassModule);

        return passModule.map(passModule1 -> new ModelAndView("modules/passModule/process", "passModule", passModule1)).orElseGet(() -> new ModelAndView("modules/passModule/process"));
    }
}
