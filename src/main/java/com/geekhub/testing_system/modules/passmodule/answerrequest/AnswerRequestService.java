package com.geekhub.testing_system.modules.passmodule.answerrequest;

import com.geekhub.testing_system.modules.passmodule.PassModule;

public interface AnswerRequestService {
    AnswerRequest save(AnswerRequest answerRequest);

    boolean checkExpirationTimeByAnswerRequest(AnswerRequest answerRequest);

    boolean checkExpirationTimeForCompleteAction(PassModule passModule);

    PassModule complete(int passModuleId);
}
