package com.geekhub.testing_system.modules.passmodule;

import com.geekhub.testing_system.admin.usermodule.UserModule;
import com.geekhub.testing_system.admin.usermodule.UserModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PassModuleServiceImpl implements PassModuleService {
    private final PassModuleRepository passModuleRepository;
    private final UserModuleService userModuleService;

    @Autowired
    public PassModuleServiceImpl(PassModuleRepository passModuleRepository, UserModuleService userModuleService) {
        this.passModuleRepository = passModuleRepository;
        this.userModuleService = userModuleService;
    }

    @Override
    public PassModule create(int userModuleId) {
        PassModule passModule = new PassModule();
        Optional<UserModule> userModule = userModuleService.findById(userModuleId);
        if (userModule.isPresent()) {
            passModule.setUserModule(userModule.get());
            return passModuleRepository.create(passModule);
        }
        return passModule;
    }

    @Override
    public Optional<PassModule> findById(int id) {
        Optional<PassModule> optionalPassModule = passModuleRepository.findById(id);

        if (optionalPassModule.isPresent()) {
            PassModule passModule = optionalPassModule.get();
            Optional<UserModule> optionalUserModule = userModuleService.findUserModuleByPassModule(passModule);

            if (optionalUserModule.isPresent()) {
                passModule.setUserModule(optionalUserModule.get());

                return Optional.of(passModule);
            }
        }

        return Optional.empty();
    }

    @Override
    public PassModule update(PassModule passModule) {
        return passModuleRepository.edit(passModule);
    }

    @Override
    public List<PassModule> findPassModulesByUserModule(UserModule userModule) {
        return passModuleRepository.findPastModulesByUserModule(userModule);
    }

    @Override
    public Optional<PassModule> findInProgressPassModuleByUserModule(int userModuleId) {
        Optional<UserModule> userModule = userModuleService.findById(userModuleId);
        if (userModule.isPresent()) {
            return passModuleRepository.findInProgressPassModuleByUserModule(userModule.get());
        }
        return Optional.empty();
    }
}
