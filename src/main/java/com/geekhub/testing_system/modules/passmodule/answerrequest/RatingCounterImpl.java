package com.geekhub.testing_system.modules.passmodule.answerrequest;

import com.geekhub.testing_system.admin.answer.Answer;
import com.geekhub.testing_system.admin.module.Module;
import com.geekhub.testing_system.modules.passmodule.PassModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class RatingCounterImpl implements RatingCounter {
    private final AnswerRequestRepository answerRequestRepository;

    @Autowired
    public RatingCounterImpl(AnswerRequestRepository answerRequestRepository) {
        this.answerRequestRepository = answerRequestRepository;
    }

    @Override
    public float count(PassModule passModule) {
        Module module = passModule.getUserModule().getModule();
        List<AnswerRequest> allAnswerRequestByPassModule = answerRequestRepository.findAllAnswerRequestByPassModule(passModule);
        int countRightAnswers = passModule.getUserModule().getModule().countAnswers();
        int countRightAnswersByPassModule = 0;

        Set<Answer> checkedAnswers = new HashSet<>();
        Set<Answer> uncheckedAnswers = new HashSet<>();

        for (Answer answer : passModule.getUserModule().getModule().getAllAnswers()) {
            for (AnswerRequest answerRequest : allAnswerRequestByPassModule) {
                if (answer.getId().equals(answerRequest.getAnswerId())) {
                    checkedAnswers.add(answer);

                    if (answer.checkIsCorrect(answerRequest.isResult())) {
                        countRightAnswersByPassModule++;
                    } else {
                        countRightAnswersByPassModule--;
                    }
                }
            }
        }

        for (Answer answer : passModule.getUserModule().getModule().getAllAnswers()) {
            if (checkedAnswers.add(answer)) {
                uncheckedAnswers.add(answer);
            }
        }

        for (Answer answer : uncheckedAnswers) {
            if (answer.isCorrectly()) {
                countRightAnswersByPassModule--;
            } else {
                countRightAnswersByPassModule++;
            }
        }

        if (countRightAnswersByPassModule < 0) {
            countRightAnswersByPassModule = 0;
        }

        float weightOneAnswer = module.getRating() / countRightAnswers;

        return weightOneAnswer * countRightAnswersByPassModule;
    }
}
