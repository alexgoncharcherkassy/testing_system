package com.geekhub.testing_system.modules.passmodule.answerrequest;

import com.geekhub.testing_system.admin.module.Module;
import com.geekhub.testing_system.admin.usermodule.ModuleStatus;
import com.geekhub.testing_system.admin.usermodule.UserModule;
import com.geekhub.testing_system.admin.usermodule.UserModuleService;
import com.geekhub.testing_system.modules.passmodule.PassModule;
import com.geekhub.testing_system.modules.passmodule.PassModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class AnswerRequestServiceImpl implements AnswerRequestService {
    private final AnswerRequestRepository answerRequestRepository;
    private final PassModuleService passModuleService;
    private final UserModuleService userModuleService;
    private final RatingCounter ratingCounter;

    @Autowired
    public AnswerRequestServiceImpl(AnswerRequestRepository answerRequestRepository, PassModuleService passModuleService, UserModuleService userModuleService, RatingCounter ratingCounter) {
        this.answerRequestRepository = answerRequestRepository;
        this.passModuleService = passModuleService;
        this.userModuleService = userModuleService;
        this.ratingCounter = ratingCounter;
    }

    @Override
    public AnswerRequest save(AnswerRequest answerRequest) {
        if (checkExpirationTimeByAnswerRequest(answerRequest)) {
            if (answerRequestRepository.findByPassModuleIdAndAnswerId(answerRequest.getPassModuleId(), answerRequest.getAnswerId()) == null) {
                return answerRequestRepository.create(answerRequest);
            } else {
                return answerRequestRepository.edit(answerRequest);
            }
        }

        return new AnswerRequest();
    }

    @Override
    public boolean checkExpirationTimeByAnswerRequest(AnswerRequest answerRequest) {
        Optional<PassModule> passModule = passModuleService.findById(answerRequest.getPassModuleId());
        return passModule.map(passModule1 -> passModule1.getExpiredAt().isAfter(LocalDateTime.now())).orElse(false);
    }

    @Override
    public boolean checkExpirationTimeForCompleteAction(PassModule passModule) {
        return passModule.getExpiredAt().isAfter(LocalDateTime.now());
    }

    @Override
    @Transactional
    public PassModule complete(int passModuleId) {
        Optional<PassModule> passModuleOptional = passModuleService.findById(passModuleId);
        if (passModuleOptional.isPresent()) {

            PassModule passModule = passModuleOptional.get();
            if (checkExpirationTimeForCompleteAction(passModule)) {
                Module module = passModule.getUserModule().getModule();

                float resultRating = ratingCounter.count(passModule);
                float resultPercentSuccess = (100 * resultRating) / module.getRating();

                passModule.setTimeEnd(LocalDateTime.now());
                passModule.setRating(resultRating);

                if (resultPercentSuccess >= module.getPercentSuccess()) {
                    passModule.setStatus(ModuleStatus.SUCCESS);
                } else {
                    passModule.setStatus(ModuleStatus.FAILED);
                }

                PassModule passModuleUpdated = passModuleService.update(passModule);

                updateUserModule(passModuleUpdated);

                return passModuleUpdated;
            }

            passModule.setStatus(ModuleStatus.FAILED);
            passModule.setTimeEnd(LocalDateTime.now());

            PassModule passModuleExpired = passModuleService.update(passModule);

            updateUserModule(passModuleExpired);

            return passModuleExpired;
        }

        return new PassModule();
    }

    private boolean isEnoughAttempts(PassModule passModule) {
        UserModule userModule = passModule.getUserModule();
        List<PassModule> pastModulesByUserModule = passModuleService.findPassModulesByUserModule(userModule);

        return pastModulesByUserModule.size() < userModule.getModule().getAttempts();
    }

    private void updateUserModule(PassModule passModule) {
        if (!isEnoughAttempts(passModule) || passModule.getStatus() == ModuleStatus.SUCCESS) {
            UserModule userModule = passModule.getUserModule();
            userModule.setStatus(passModule.getStatus());
            userModule.setRating(passModule.getRating());
            userModule.setFinishedAt(passModule.getTimeEnd());

            userModuleService.update(userModule);
        }
    }
}
