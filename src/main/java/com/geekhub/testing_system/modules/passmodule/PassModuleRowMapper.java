package com.geekhub.testing_system.modules.passmodule;

import com.geekhub.testing_system.admin.usermodule.ModuleStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PassModuleRowMapper implements RowMapper<PassModule> {
    @Override
    public PassModule mapRow(ResultSet rs, int rowNum) throws SQLException {
        PassModule passModule = new PassModule();
        passModule.setId(rs.getInt("id"));
        passModule.setRating(rs.getFloat("rating"));
        passModule.setStatus(ModuleStatus.valueOf(rs.getString("status")));
        passModule.setTimeStart(rs.getTimestamp("time_start").toLocalDateTime());
        passModule.setTimeEnd(rs.getTimestamp("time_end") != null ? rs.getTimestamp("time_end").toLocalDateTime() : null);

        return passModule;
    }
}
