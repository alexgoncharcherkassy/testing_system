package com.geekhub.testing_system.modules.passmodule;

import com.geekhub.testing_system.admin.usermodule.UserModule;

import java.util.List;
import java.util.Optional;

public interface PassModuleRepository {
    PassModule create(PassModule passModule);

    PassModule edit(PassModule passModule);

    Optional<PassModule> findById(int id);

    List<PassModule> findPastModulesByUserModule(UserModule userModule);

    Optional<PassModule> findInProgressPassModuleByUserModule(UserModule userModule);
}
