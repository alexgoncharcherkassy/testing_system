package com.geekhub.testing_system.modules.passmodule;

import com.geekhub.testing_system.admin.usermodule.ModuleStatus;
import com.geekhub.testing_system.admin.usermodule.UserModule;

import java.time.LocalDateTime;

public class PassModule {
    private Integer id;
    private Float rating;
    private ModuleStatus status;
    private LocalDateTime timeStart;
    private LocalDateTime timeEnd;
    private UserModule userModule;

    public PassModule() {
        setStatus(ModuleStatus.IN_PROGRESS);
        setTimeStart(LocalDateTime.now());
        setRating(0.0F);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public ModuleStatus getStatus() {
        return status;
    }

    public void setStatus(ModuleStatus status) {
        this.status = status;
    }

    public LocalDateTime getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(LocalDateTime timeStart) {
        this.timeStart = timeStart;
    }

    public LocalDateTime getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(LocalDateTime timeEnd) {
        this.timeEnd = timeEnd;
    }

    public UserModule getUserModule() {
        return userModule;
    }

    public void setUserModule(UserModule userModule) {
        this.userModule = userModule;
    }

    public LocalDateTime getExpiredAt() {
        return timeStart.plusMinutes(userModule.getModule().getTime());
    }

    public float getRatingPercent() {
        return (rating * 100) / userModule.getModule().getRating();
    }
}
