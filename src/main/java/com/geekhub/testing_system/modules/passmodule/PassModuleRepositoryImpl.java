package com.geekhub.testing_system.modules.passmodule;

import com.geekhub.testing_system.admin.usermodule.ModuleStatus;
import com.geekhub.testing_system.admin.usermodule.UserModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class PassModuleRepositoryImpl implements PassModuleRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PassModuleRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public PassModule create(PassModule passModule) {
        String sql = "INSERT INTO pass_module (rating, status, time_start, user_module_id) VALUES (?, ?, ?, ?);";

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(c -> {
            PreparedStatement preparedStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setFloat(1, passModule.getRating());
            preparedStatement.setString(2, passModule.getStatus().toString());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(passModule.getTimeStart()));
            preparedStatement.setInt(4, passModule.getUserModule().getId());

            return preparedStatement;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();

        for (Map.Entry<String, Object> entry : keys.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("id")) {
                passModule.setId((Integer) entry.getValue());
            }
        }

        return passModule;
    }

    @Override
    public Optional<PassModule> findById(int id) {
        String sql = "SELECT id, rating, status, time_start, time_end, user_module_id FROM pass_module WHERE id = ?;";

        try {
            PassModule passModule = jdbcTemplate.queryForObject(sql, new Object[]{id}, new PassModuleRowMapper());
            return Optional.of(passModule);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public PassModule edit(PassModule passModule) {
        String sql = "UPDATE pass_module SET rating = ?, time_end = ?, status = ? WHERE id = ?;";

        try {
            jdbcTemplate.update(sql, passModule.getRating(), passModule.getTimeEnd(), passModule.getStatus().name(), passModule.getId());
            return passModule;
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public List<PassModule> findPastModulesByUserModule(UserModule userModule) {
        String sql = "SELECT pm.id, pm.rating, pm.status, pm.time_start, pm.time_end, pm.user_module_id FROM pass_module pm " +
                "JOIN user_module m2 ON pm.user_module_id = m2.id WHERE m2.id = ?;";

        try {
            return jdbcTemplate.query(sql, new Object[]{userModule.getId()}, new PassModuleRowMapper());
        } catch (DataAccessException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<PassModule> findInProgressPassModuleByUserModule(UserModule userModule) {
        String sql = "SELECT pm.id, pm.rating, pm.status, pm.time_start, pm.time_end, pm.user_module_id FROM pass_module pm " +
                "JOIN user_module m2 ON pm.user_module_id = m2.id WHERE m2.id = ? AND pm.status = ?;";

        try {
            PassModule passModule = jdbcTemplate.queryForObject(sql, new Object[]{userModule.getId(), ModuleStatus.IN_PROGRESS.name()}, new PassModuleRowMapper());
            return Optional.of(passModule);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }
}
