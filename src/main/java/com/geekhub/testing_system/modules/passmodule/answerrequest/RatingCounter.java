package com.geekhub.testing_system.modules.passmodule.answerrequest;

import com.geekhub.testing_system.modules.passmodule.PassModule;

public interface RatingCounter {
    float count(PassModule passModule);
}
