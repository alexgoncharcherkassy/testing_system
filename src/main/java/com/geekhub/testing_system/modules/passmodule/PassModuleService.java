package com.geekhub.testing_system.modules.passmodule;

import com.geekhub.testing_system.admin.usermodule.UserModule;

import java.util.List;
import java.util.Optional;

public interface PassModuleService {
    PassModule create(int userModuleId);

    PassModule update(PassModule passModule);

    Optional<PassModule> findById(int id);

    List<PassModule> findPassModulesByUserModule(UserModule userModule);

    Optional<PassModule> findInProgressPassModuleByUserModule(int userModuleId);
}
