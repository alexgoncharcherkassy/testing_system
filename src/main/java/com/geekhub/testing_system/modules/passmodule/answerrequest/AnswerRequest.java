package com.geekhub.testing_system.modules.passmodule.answerrequest;

public class AnswerRequest {
    private Integer id;
    private Integer passModuleId;
    private Integer answerId;
    private boolean result;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPassModuleId() {
        return passModuleId;
    }

    public void setPassModuleId(Integer passModuleId) {
        this.passModuleId = passModuleId;
    }

    public Integer getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
