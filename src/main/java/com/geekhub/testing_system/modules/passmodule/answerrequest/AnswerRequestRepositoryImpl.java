package com.geekhub.testing_system.modules.passmodule.answerrequest;

import com.geekhub.testing_system.modules.passmodule.PassModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

@Repository
public class AnswerRequestRepositoryImpl implements AnswerRequestRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AnswerRequestRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public AnswerRequest create(AnswerRequest answerRequest) {
        String sql = "INSERT INTO answer_request (answer_id, pass_module_id, result) VALUES (?, ?, ?);";

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(c -> {
            PreparedStatement preparedStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, answerRequest.getAnswerId());
            preparedStatement.setInt(2, answerRequest.getPassModuleId());
            preparedStatement.setBoolean(3, answerRequest.isResult());

            return preparedStatement;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();

        for (Map.Entry<String, Object> entry : keys.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("id")) {
                answerRequest.setId((Integer) entry.getValue());
            }
        }

        return answerRequest;
    }

    @Override
    public AnswerRequest edit(AnswerRequest answerRequest) {
        String sql = "UPDATE answer_request SET result = ? WHERE answer_id = ? AND pass_module_id = ?;";

        try {
            jdbcTemplate.update(sql, answerRequest.isResult(), answerRequest.getAnswerId(), answerRequest.getPassModuleId());

            return answerRequest;
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public AnswerRequest findByPassModuleIdAndAnswerId(int passModuleId, int answerId) {
        String sql = "SELECT ar.id, ar.answer_id, ar.pass_module_id, ar.result FROM answer_request ar " +
                "JOIN pass_module m2 ON ar.pass_module_id = m2.id WHERE answer_id = ? AND m2.id = ?;";

        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{answerId, passModuleId}, new AnswerRequestRowMapper());
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public List<AnswerRequest> findAllAnswerRequestByPassModule(PassModule passModule) {
        String sql = "SELECT ar.id, ar.answer_id, ar.pass_module_id, ar.result FROM answer_request ar " +
                "JOIN pass_module m2 ON ar.pass_module_id = m2.id WHERE m2.id = ?;";

        try {
            return jdbcTemplate.query(sql, new Object[]{passModule.getId()}, new AnswerRequestRowMapper());
        } catch (DataAccessException e) {
            return null;
        }
    }
}
