package com.geekhub.testing_system.modules;

import com.geekhub.testing_system.admin.usermodule.UserModuleService;
import com.geekhub.testing_system.user.User;
import com.geekhub.testing_system.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping("/modules")
public class AssignedModuleController {
    private final UserModuleService userModuleService;
    private final UserService userService;

    @Autowired
    public AssignedModuleController(UserModuleService userModuleService, UserService userService) {
        this.userModuleService = userModuleService;
        this.userService = userService;
    }

    @GetMapping("")
    public ModelAndView assignedModulesPage(Principal principal) {
        Optional<User> user = userService.findByUsername(principal.getName());
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("User not found");
        }

        return new ModelAndView("modules/index", "modules", userModuleService.findInProgressUserModuleByUser(user.get()));
    }

    @GetMapping("/result")
    public ModelAndView resultModulesPage(Principal principal) {
        Optional<User> user = userService.findByUsername(principal.getName());
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("User not found");
        }

        return new ModelAndView("modules/result", "modules", userModuleService.findCompletedUserModuleByUser(user.get()));
    }
}
