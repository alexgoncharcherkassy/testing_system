package com.geekhub.testing_system.admin.student;

import com.geekhub.testing_system.admin.module.Module;
import com.geekhub.testing_system.admin.module.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin/students")
public class StudentController {
    private final StudentService studentService;
    private final ModuleService moduleService;

    @Autowired
    public StudentController(StudentService studentService, ModuleService moduleService) {
        this.studentService = studentService;
        this.moduleService = moduleService;
    }

    @GetMapping("")
    public ModelAndView allStudentsPage() {
        ModelAndView modelAndView = new ModelAndView("/admin/student/index");

        modelAndView.addObject("students", studentService.findAllStudents());

        List<Module> modules = moduleService.findAll();
        List<Module> activeModules = modules.stream()
                .filter(Module::isActiveModule)
                .collect(Collectors.toList());

        modelAndView.addObject("modules", activeModules);

        return modelAndView;
    }
}
