package com.geekhub.testing_system.admin.student;

import com.geekhub.testing_system.user.User;

import java.util.List;

public interface StudentService {
    List<User> findAllStudents();
}
