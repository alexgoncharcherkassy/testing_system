package com.geekhub.testing_system.admin.student;

import com.geekhub.testing_system.admin.usermodule.UserModule;
import com.geekhub.testing_system.admin.usermodule.UserModuleService;
import com.geekhub.testing_system.user.User;
import com.geekhub.testing_system.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    private final UserRepository userRepository;
    private final UserModuleService userModuleService;

    @Autowired
    public StudentServiceImpl(UserRepository userRepository, UserModuleService userModuleService) {
        this.userRepository = userRepository;
        this.userModuleService = userModuleService;
    }

    @Override
    public List<User> findAllStudents() {
        List<User> users = userRepository.findAllUsersByRole("ROLE_USER");
        Collection<Integer> ids = users.stream()
                .map(User::getId)
                .collect(Collectors.toList());

        Map<Integer, Collection<UserModule>> allUserModulesByUserIds = userModuleService.findAllUserModulesByUserIds(ids, null);

        for (User user : users) {
            if (allUserModulesByUserIds.containsKey(user.getId())) {
                user.setUserModules(allUserModulesByUserIds.get(user.getId()));
            }
        }

        return users;
    }
}
