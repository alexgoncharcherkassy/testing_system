package com.geekhub.testing_system.admin.question;

import com.geekhub.testing_system.exception.ResourceValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/admin/modules/{id}/questions")
public class QuestionController {
    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @PostMapping("")
    public Question create(@PathVariable Integer id, @Valid Question question, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResourceValidationException("Question", result.getFieldError().getField(), result.getFieldError().getDefaultMessage());
        }

        return questionService.save(question, id);
    }

    @GetMapping("")
    public List<Question> questionsByModule(@PathVariable Integer id) {
        return questionService.findQuestionsByModule(id);
    }
}
