package com.geekhub.testing_system.admin.question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

@Repository
public class QuestionRepositoryImpl implements QuestionRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public QuestionRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Question create(Question question, int moduleId) {
        String sql = "INSERT INTO public.question (title, module_id) VALUES (?, ?);";

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(c -> {
            PreparedStatement preparedStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, question.getTitle());
            preparedStatement.setInt(2, moduleId);

            return preparedStatement;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();

        for (Map.Entry<String, Object> entry : keys.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("id")) {
                question.setId((Integer) entry.getValue());
            }
        }

        return question;
    }

    @Override
    public Question edit(Question question) {
        String sql = "UPDATE public.question SET title = ? WHERE id = ?;";

        try {
            jdbcTemplate.update(sql, question.getTitle(), question.getId());
        } catch (DataAccessException e) {
        }

        return question;
    }

    @Override
    public Optional<Question> findById(int id) {
        String sql = "SELECT id, title, module_id FROM public.question WHERE id = ?;";

        try {
            Question question = jdbcTemplate.queryForObject(sql, new Object[]{id}, new QuestionRowMapper());

            return Optional.of(question);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean delete(int id) {
        String sql = "DELETE FROM public.question WHERE id = ?;";

        int updateRows = jdbcTemplate.update(sql, id);

        return updateRows > 0;
    }

    @Override
    public List<Question> findQuestionsByModule(int id) {
        String sql = "SELECT id, title, module_id FROM public.question WHERE module_id = ?;";

        return jdbcTemplate.query(sql, new Object[]{id}, new QuestionRowMapper());
    }

    @Override
    public Map<Integer, Collection<Question>> findAllQuestionsByModuleIds(Collection<Integer> moduleIds) {
        if (moduleIds.size() == 0) {
            return new HashMap<>();
        }

        String sql = "SELECT id, title, module_id FROM public.question WHERE module_id IN (:ids);";

        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", moduleIds);

        return template.query(sql, parameters, (ResultSet rs) -> {
            Map<Integer, Collection<Question>> questionsByModuleId = new HashMap<>();

            while (rs.next()) {
                Question question = new Question();
                question.setId(rs.getInt("id"));
                question.setModuleId(rs.getInt("module_id"));
                question.setTitle(rs.getString("title"));

                questionsByModuleId.computeIfPresent(question.getModuleId(), (k, v) -> {
                    v.add(question);
                    return v;
                });
                questionsByModuleId.computeIfAbsent(question.getModuleId(), k -> new ArrayList<>(Collections.singletonList(question)));

            }

            return questionsByModuleId;
        });
    }
}
