package com.geekhub.testing_system.admin.question;

import com.geekhub.testing_system.admin.answer.Answer;
import com.geekhub.testing_system.admin.answer.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepository questionRepository;
    private final AnswerService answerService;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository, AnswerService answerService) {
        this.questionRepository = questionRepository;
        this.answerService = answerService;
    }

    @Override
    public Question save(Question question, int module_id) {
        return question.isNew() ? questionRepository.create(question, module_id) : questionRepository.edit(question);
    }

    @Override
    public Optional<Question> findById(int id) {
        return questionRepository.findById(id);
    }

    @Override
    public boolean delete(int id) {
        return questionRepository.delete(id);
    }

    @Override
    public List<Question> findQuestionsByModule(int id) {
        return questionRepository.findQuestionsByModule(id);
    }

    @Override
    public Map<Integer, Collection<Question>> findAllQuestionsByModuleIds(Collection<Integer> moduleIds) {
        Map<Integer, Collection<Question>> allQuestionsByModuleIds = questionRepository.findAllQuestionsByModuleIds(moduleIds);
        Collection<Integer> questionIds = new ArrayList<>();
        for (Map.Entry<Integer, Collection<Question>> entry : allQuestionsByModuleIds.entrySet()) {
            questionIds.addAll(entry.getValue().stream().map(Question::getId).collect(Collectors.toList()));
        }

        Map<Integer, Collection<Answer>> allAnswersByQuestionIds = answerService.findAllAnswersByQuestionIds(questionIds);

        for (Map.Entry<Integer, Collection<Question>> entry : allQuestionsByModuleIds.entrySet()) {
            Collection<Question> questions = entry.getValue();
            for (Question question : questions) {
                if (allAnswersByQuestionIds.containsKey(question.getId())) {
                    question.setAnswers(allAnswersByQuestionIds.get(question.getId()));
                }
            }
        }

        return allQuestionsByModuleIds;
    }
}
