package com.geekhub.testing_system.admin.question;

import com.geekhub.testing_system.admin.answer.Answer;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Question {
    private Integer id;

    @NotBlank(message = "Title could not be empty")
    @NotNull
    @Length(max = 2500)
    private String title;

    private Integer moduleId;

    private Collection<Answer> answers = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public boolean isNew() {
        return id == null;
    }

    public Collection<Answer> getAnswers() {
        return new ArrayList<>(answers);
    }

    public void setAnswers(Collection<Answer> answers) {
        this.answers = answers;
    }

    public boolean isActiveQuestion() {
        return answers.size() >= 2;
    }
}
