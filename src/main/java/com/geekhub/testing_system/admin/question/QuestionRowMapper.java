package com.geekhub.testing_system.admin.question;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class QuestionRowMapper implements RowMapper<Question> {
    @Override
    public Question mapRow(ResultSet rs, int rowNum) throws SQLException {
        Question question = new Question();
        question.setTitle(rs.getString("title"));
        question.setId(rs.getInt("id"));
        question.setModuleId(rs.getInt("module_id"));

        return question;
    }
}
