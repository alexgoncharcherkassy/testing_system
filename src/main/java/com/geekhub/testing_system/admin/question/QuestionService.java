package com.geekhub.testing_system.admin.question;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface QuestionService {
    Question save(Question question, int moduleId);

    Optional<Question> findById(int id);

    boolean delete(int id);

    List<Question> findQuestionsByModule(int id);

    Map<Integer, Collection<Question>> findAllQuestionsByModuleIds(Collection<Integer> moduleIds);
}
