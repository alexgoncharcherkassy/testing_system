package com.geekhub.testing_system.admin.defaultpage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DefaultAdminController {
    @GetMapping("/admin")
    public ModelAndView defaultPage() {
        return new ModelAndView("admin/default/index");
    }
}
