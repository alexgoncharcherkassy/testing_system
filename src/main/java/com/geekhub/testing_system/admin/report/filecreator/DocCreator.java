package com.geekhub.testing_system.admin.report.filecreator;

import com.geekhub.testing_system.admin.report.ReportDTO;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

@Service
@Qualifier("DocCreator")
public class DocCreator implements FileCreator {
    private Collection<ReportDTO> reports;

    @Override
    public byte[] generate(Collection<ReportDTO> reports) {
        this.reports = reports;

        try (XWPFDocument document = new XWPFDocument();
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        ) {

            XWPFTable table = document.createTable();

            addTableHeader(table);
            addRows(table);

            document.write(outputStream);

            return outputStream.toByteArray();
        } catch (Exception e) {
            return new byte[1];
        }
    }

    private void addTableHeader(XWPFTable table) {
        XWPFTableRow tableRowOne = table.getRow(0);
        tableRowOne.getCell(0).setText("First Name");
        tableRowOne.addNewTableCell().setText("Last Name");
        tableRowOne.addNewTableCell().setText("Username");
        tableRowOne.addNewTableCell().setText("Module Title");
        tableRowOne.addNewTableCell().setText("Rating");
        tableRowOne.addNewTableCell().setText("Percent");
        tableRowOne.addNewTableCell().setText("Finished At");
    }

    private void addRows(XWPFTable table) {
        for (ReportDTO report : reports) {
            XWPFTableRow tableRowTwo = table.createRow();
            tableRowTwo.getCell(0).setText(report.getUser().getFirstName());
            tableRowTwo.getCell(1).setText(report.getUser().getLastName());
            tableRowTwo.getCell(2).setText(report.getUser().getUsername());
            tableRowTwo.getCell(3).setText(report.getUserModule().getModule().getTitle());
            tableRowTwo.getCell(4).setText(String.format("%.2f%n", report.getUserModule().getRating()));
            tableRowTwo.getCell(5).setText(String.format("%.2f%n", report.getUserModule().getRatingPercent()));
            tableRowTwo.getCell(6).setText(report.getUserModule().getFinishedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }
    }
}
