package com.geekhub.testing_system.admin.report.filecreator;

import com.geekhub.testing_system.admin.report.ReportDTO;

import java.util.Collection;

public interface FileCreator {
    byte[] generate(Collection<ReportDTO> reports);
}
