package com.geekhub.testing_system.admin.report;

import com.geekhub.testing_system.admin.usermodule.ModuleStatus;
import com.geekhub.testing_system.admin.usermodule.UserModule;
import com.geekhub.testing_system.admin.usermodule.UserModuleService;
import com.geekhub.testing_system.user.User;
import com.geekhub.testing_system.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {
    private final UserRepository userRepository;
    private final UserModuleService userModuleService;

    @Autowired
    public ReportServiceImpl(UserRepository userRepository, UserModuleService userModuleService) {
        this.userRepository = userRepository;
        this.userModuleService = userModuleService;
    }

    @Override
    public List<ReportDTO> findAllStudentsByModuleStatus(ModuleStatus status, Integer moduleId) {
        List<User> users = userRepository.findAllUsersByRole("ROLE_USER");
        Collection<Integer> ids = users.stream()
                .map(User::getId)
                .collect(Collectors.toList());

        Map<Integer, Collection<UserModule>> allUserModulesByUserIds = userModuleService.findAllUserModulesByUserIds(ids, status);

        List<ReportDTO> report = new ArrayList<>();

        for (User user : users) {
            if (allUserModulesByUserIds.containsKey(user.getId())) {
                for (UserModule userModule : allUserModulesByUserIds.get(user.getId())) {
                    ReportDTO reportDTO = new ReportDTO();
                    reportDTO.setUser(user);
                    reportDTO.setUserModule(userModule);

                    report.add(reportDTO);
                }
            }
        }

        return report.stream()
                .filter(reportDTO -> reportDTO.getUserModule().getModule().getId().equals(moduleId))
                .sorted((r1, r2) -> Float.compare(r2.getUserModule().getRating(), r1.getUserModule().getRating()))
                .collect(Collectors.toList());
    }
}
