package com.geekhub.testing_system.admin.report;

import com.geekhub.testing_system.admin.usermodule.UserModule;
import com.geekhub.testing_system.user.User;

public class ReportDTO {
    private User user;
    private UserModule userModule;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserModule getUserModule() {
        return userModule;
    }

    public void setUserModule(UserModule userModule) {
        this.userModule = userModule;
    }
}
