package com.geekhub.testing_system.admin.report.filecreator;

import com.geekhub.testing_system.admin.report.ReportDTO;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.stream.Stream;

@Service
@Qualifier("PdfCreator")
public class PdfCreator implements FileCreator {
    private Collection<ReportDTO> reports;

    @Override
    public byte[] generate(Collection<ReportDTO> reports) {
        this.reports = reports;

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            Document document = new Document();

            PdfWriter.getInstance(document, outputStream);

            document.open();

            PdfPTable table = new PdfPTable(7);
            addTableHeader(table);
            addRows(table);

            document.add(table);
            document.close();

            return outputStream.toByteArray();
        } catch (Exception e) {
            return new byte[1];
        }
    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("First Name", "Last Name", "Username", "Module Title", "Rating", "Percent", "Finished At")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addRows(PdfPTable table) {
        for (ReportDTO report : reports) {
            table.addCell(report.getUser().getFirstName());
            table.addCell(report.getUser().getLastName());
            table.addCell(report.getUser().getUsername());
            table.addCell(report.getUserModule().getModule().getTitle());
            table.addCell(String.format("%.2f%n", report.getUserModule().getRating()));
            table.addCell(String.format("%.2f%n", report.getUserModule().getRatingPercent()));
            table.addCell(report.getUserModule().getFinishedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }
    }
}
