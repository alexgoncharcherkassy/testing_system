package com.geekhub.testing_system.admin.report;

import com.geekhub.testing_system.admin.module.ModuleService;
import com.geekhub.testing_system.admin.report.filecreator.FileCreator;
import com.geekhub.testing_system.admin.usermodule.ModuleStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("admin/report")
public class ReportController {
    private final ReportService reportService;
    private final ModuleService moduleService;
    private final FileCreator pdfCreator;
    private final FileCreator xlsCreator;
    private final FileCreator docCreator;

    @Autowired
    public ReportController(ReportService reportService, ModuleService moduleService,
                            @Qualifier("PdfCreator") FileCreator pdfCreator,
                            @Qualifier("XlsCreator") FileCreator xlsCreator,
                            @Qualifier("DocCreator") FileCreator docCreator) {

        this.reportService = reportService;
        this.moduleService = moduleService;
        this.pdfCreator = pdfCreator;
        this.xlsCreator = xlsCreator;
        this.docCreator = docCreator;
    }

    @GetMapping("/success")
    public ModelAndView showSuccessResults(@RequestParam(value = "module", required = false) Integer moduleId) {
        ModelAndView modelAndView = new ModelAndView("/admin/report/success");
        modelAndView.addObject("report", reportService.findAllStudentsByModuleStatus(ModuleStatus.SUCCESS, moduleId));
        modelAndView.addObject("modules", moduleService.findAll());

        return modelAndView;
    }

    @GetMapping("/failed")
    public ModelAndView showFailedResults(@RequestParam(value = "module", required = false) Integer moduleId) {
        ModelAndView modelAndView = new ModelAndView("/admin/report/failed");
        modelAndView.addObject("report", reportService.findAllStudentsByModuleStatus(ModuleStatus.FAILED, moduleId));
        modelAndView.addObject("modules", moduleService.findAll());

        return modelAndView;
    }

    @GetMapping("/getPDF")
    public ResponseEntity<byte[]> getPdf(@RequestParam(value = "module", required = false) Integer moduleId,
                                         @RequestParam(value = "status", required = false) String status) {

        HttpHeaders headers = new HttpHeaders();
        byte[] content = pdfCreator.generate(reportService.findAllStudentsByModuleStatus(ModuleStatus.valueOf(status.toUpperCase()), moduleId));
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "reportPdf.pdf";
        headers.setContentDispositionFormData(filename, filename);

        return new ResponseEntity<>(content, headers, HttpStatus.OK);
    }

    @GetMapping("/getXLS")
    public ResponseEntity<byte[]> getXls(@RequestParam(value = "module", required = false) Integer moduleId,
                                         @RequestParam(value = "status", required = false) String status) {

        HttpHeaders headers = new HttpHeaders();
        byte[] content = xlsCreator.generate(reportService.findAllStudentsByModuleStatus(ModuleStatus.valueOf(status.toUpperCase()), moduleId));
        headers.setContentType(MediaType.parseMediaType("application/xls"));
        String filename = "reportXls.xls";
        headers.setContentDispositionFormData(filename, filename);

        return new ResponseEntity<>(content, headers, HttpStatus.OK);
    }

    @GetMapping("/getDOC")
    public ResponseEntity<byte[]> getDoc(@RequestParam(value = "module", required = false) Integer moduleId,
                                         @RequestParam(value = "status", required = false) String status) {

        HttpHeaders headers = new HttpHeaders();
        byte[] content = docCreator.generate(reportService.findAllStudentsByModuleStatus(ModuleStatus.valueOf(status.toUpperCase()), moduleId));
        headers.setContentType(MediaType.parseMediaType("application/doc"));
        String filename = "reportDoc.doc";
        headers.setContentDispositionFormData(filename, filename);

        return new ResponseEntity<>(content, headers, HttpStatus.OK);
    }
}
