package com.geekhub.testing_system.admin.report.filecreator;

import com.geekhub.testing_system.admin.report.ReportDTO;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

@Service
@Qualifier("XlsCreator")
public class XlsCreator implements FileCreator {
    private Collection<ReportDTO> reports;

    @Override
    public byte[] generate(Collection<ReportDTO> reports) {
        this.reports = reports;

        try (Workbook book = new HSSFWorkbook();
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        ) {

            Sheet sheet = book.createSheet("Report");

            addTableHeader(sheet);
            addRows(sheet);

            book.write(outputStream);

            return outputStream.toByteArray();
        } catch (IOException e) {
            return new byte[1];
        }
    }

    private void addTableHeader(Sheet sheet) {
        Row row = sheet.createRow(0);

        Cell firstName = row.createCell(0);
        firstName.setCellValue("First Name");
        Cell lastName = row.createCell(1);
        lastName.setCellValue("Last Name");
        Cell username = row.createCell(2);
        username.setCellValue("Username");
        Cell title = row.createCell(3);
        title.setCellValue("Module Title");
        Cell rating = row.createCell(4);
        rating.setCellValue("Rating");
        Cell percent = row.createCell(5);
        percent.setCellValue("Percent");
        Cell finishedAt = row.createCell(6);
        finishedAt.setCellValue("Finished At");
    }

    private void addRows(Sheet sheet) {
        int rows = 1;

        for (ReportDTO report : reports) {
            Row row = sheet.createRow(rows);
            Cell firstName = row.createCell(0);
            firstName.setCellValue(report.getUser().getFirstName());
            Cell lastName = row.createCell(1);
            lastName.setCellValue(report.getUser().getLastName());
            Cell username = row.createCell(2);
            username.setCellValue(report.getUser().getUsername());
            Cell title = row.createCell(3);
            title.setCellValue(report.getUserModule().getModule().getTitle());
            Cell rating = row.createCell(4);
            rating.setCellValue(String.format("%.2f%n", report.getUserModule().getRating()));
            Cell percent = row.createCell(5);
            percent.setCellValue(String.format("%.2f%n", report.getUserModule().getRatingPercent()));
            Cell finishedAt = row.createCell(6);
            finishedAt.setCellValue(report.getUserModule().getFinishedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            rows++;
        }
    }
}
