package com.geekhub.testing_system.admin.report;

import com.geekhub.testing_system.admin.usermodule.ModuleStatus;

import java.util.List;

public interface ReportService {
    List<ReportDTO> findAllStudentsByModuleStatus(ModuleStatus status, Integer moduleId);
}
