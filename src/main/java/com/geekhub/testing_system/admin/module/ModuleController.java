package com.geekhub.testing_system.admin.module;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/admin/modules")
public class ModuleController {
    private final ModuleService moduleService;

    public ModuleController(ModuleService moduleService) {
        this.moduleService = moduleService;
    }

    @GetMapping("")
    public ModelAndView modulesPage() {
        return new ModelAndView("admin/module/index", "modules", moduleService.findAll());
    }

    @GetMapping("/new")
    public ModelAndView createPage() {
        return new ModelAndView("admin/module/moduleForm", "module", new Module());
    }

    @GetMapping("/{id}/edit")
    public ModelAndView editPage(@PathVariable() Integer id) {
        Optional<Module> optionalModule = moduleService.findById(id);

        return optionalModule.map(module -> new ModelAndView("admin/module/moduleForm", "module", module)).orElseGet(() -> new ModelAndView("redirect:/admin/modules"));

    }

    @PostMapping("/save")
    public ModelAndView save(@Valid Module module, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("admin/module/moduleForm", "module", module);
        }

        moduleService.save(module);

        return new ModelAndView("redirect:/admin/modules/" + module.getId() + "/edit");
    }
}
