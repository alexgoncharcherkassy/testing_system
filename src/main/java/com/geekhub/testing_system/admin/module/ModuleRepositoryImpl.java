package com.geekhub.testing_system.admin.module;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.*;

@Repository
public class ModuleRepositoryImpl implements ModuleRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ModuleRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Module create(Module module) {
        String sql = "INSERT INTO public.module (title, rating, percent_success, attempts, time, created_at) VALUES (?, ?, ?, ?, ?, ?);";

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(c -> {
            PreparedStatement preparedStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, module.getTitle());
            preparedStatement.setFloat(2, module.getRating());
            preparedStatement.setFloat(3, module.getPercentSuccess());
            preparedStatement.setInt(4, module.getAttempts());
            preparedStatement.setInt(5, module.getTime());
            preparedStatement.setTimestamp(6, Timestamp.valueOf(module.getCreatedAt()));

            return preparedStatement;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();

        for (Map.Entry<String, Object> entry : keys.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("id")) {
                module.setId((Integer) entry.getValue());
            }
        }
        return module;
    }

    @Override
    public Module edit(Module module) {
        String sql = "UPDATE public.module SET title = ?, time = ?, rating = ?, percent_success = ?, attempts = ? WHERE id = ?;";

        try {
            jdbcTemplate.update(sql, module.getTitle(), module.getTime(), module.getRating(), module.getPercentSuccess(), module.getAttempts(), module.getId());
        } catch (DataAccessException e) {
        }

        return module;
    }

    @Override
    public Optional<Module> findById(int id) {
        String sql = "SELECT id, title, time, rating, percent_success, attempts, created_at FROM public.module WHERE id = ?;";

        try {
            Module module = jdbcTemplate.queryForObject(sql, new Object[]{id}, new ModuleRowMapper());

            return Optional.of(module);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Module> findAll() {
        String sql = "SELECT id, title, time, rating, percent_success, attempts, created_at FROM public.module ORDER BY title ASC;";

        return jdbcTemplate.query(sql, new ModuleRowMapper());
    }

    @Override
    public Map<Integer, Module> findAllModulesByUserModuleIds(Collection<Integer> userModuleIds) {
        if (userModuleIds.size() == 0) {
            return new HashMap<>();
        }

        String sql = "SELECT m2.id, m2.title, m2.time, m2.rating, m2.percent_success, m2.attempts, m2.created_at, um.id AS user_module_id FROM public.module m2 " +
                "JOIN user_module um ON m2.id = um.module_id WHERE um.id IN (:ids);";

        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", userModuleIds);

        return template.query(sql, parameters, (ResultSet rs) -> {
            Map<Integer, Module> moduleByUserModuleId = new HashMap<>();

            while (rs.next()) {
                Module module = new Module();
                module.setAttempts(rs.getInt("attempts"));
                module.setId(rs.getInt("id"));
                module.setCreatedAt(rs.getTimestamp("created_at").toLocalDateTime());
                module.setPercentSuccess(rs.getFloat("percent_success"));
                module.setRating(rs.getFloat("rating"));
                module.setTime(rs.getInt("time"));
                module.setTitle(rs.getString("title"));

                moduleByUserModuleId.put(rs.getInt("user_module_id"), module);
            }

            return moduleByUserModuleId;
        });
    }

    @Override
    public List<Module> findAllModulesIds(Collection<Integer> moduleIds) {
        if (moduleIds.size() == 0) {
            return new ArrayList<>();
        }

        String sql = "SELECT id, title, time, rating, percent_success, attempts, created_at FROM public.module m2 WHERE id IN (:ids);";

        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", moduleIds);

        return template.query(sql, parameters, (ResultSet rs) -> {
            List<Module> moduleById = new ArrayList<>();

            while (rs.next()) {
                Module module = new Module();
                module.setAttempts(rs.getInt("attempts"));
                module.setId(rs.getInt("id"));
                module.setCreatedAt(rs.getTimestamp("created_at").toLocalDateTime());
                module.setPercentSuccess(rs.getFloat("percent_success"));
                module.setRating(rs.getFloat("rating"));
                module.setTime(rs.getInt("time"));
                module.setTitle(rs.getString("title"));

                moduleById.add(module);
            }

            return moduleById;
        });
    }
}
