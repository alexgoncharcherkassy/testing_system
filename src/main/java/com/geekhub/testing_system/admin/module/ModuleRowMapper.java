package com.geekhub.testing_system.admin.module;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ModuleRowMapper implements RowMapper<Module> {
    @Override
    public Module mapRow(ResultSet rs, int rowNum) throws SQLException {
        Module module = new Module();
        module.setAttempts(rs.getInt("attempts"));
        module.setId(rs.getInt("id"));
        module.setCreatedAt(rs.getTimestamp("created_at").toLocalDateTime());
        module.setPercentSuccess(rs.getFloat("percent_success"));
        module.setRating(rs.getFloat("rating"));
        module.setTime(rs.getInt("time"));
        module.setTitle(rs.getString("title"));

        return module;
    }
}
