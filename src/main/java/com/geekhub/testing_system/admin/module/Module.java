package com.geekhub.testing_system.admin.module;

import com.geekhub.testing_system.admin.answer.Answer;
import com.geekhub.testing_system.admin.question.Question;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Module {
    private Integer id;
    @NotNull
    @NotBlank(message = "Title could not be empty")
    @Length(max = 100, message = "Title could not be greater than 100 characters")
    private String title;

    @NotNull
    @Min(value = 0, message = "Rating should be greater than zero")
    private Float rating;

    @NotNull
    @Min(value = 0, message = "Time should be greater than zero")
    private Integer time;

    @NotNull
    @Min(value = 0, message = "Percent should be greater than zero")
    @Max(value = 100, message = "Percent should be less or equal 100")
    private Float percentSuccess;

    @NotNull
    @Min(value = 1, message = "Attempts should be greater or equal than one")
    private Integer attempts;

    private LocalDateTime createdAt;

    private Collection<Question> questions = new ArrayList<>();

    public Module() {
        this.setCreatedAt(LocalDateTime.now());
    }

    public boolean isNew() {
        return id == null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Float getPercentSuccess() {
        return percentSuccess;
    }

    public void setPercentSuccess(Float percentSuccess) {
        this.percentSuccess = percentSuccess;
    }

    public Integer getAttempts() {
        return attempts;
    }

    public void setAttempts(Integer attempts) {
        this.attempts = attempts;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setQuestions(Collection<Question> questions) {
        this.questions = questions;
    }

    public Collection<Question> getQuestions() {
        return new ArrayList<>(questions);
    }

    public int countAnswers() {
        return questions.stream()
                .mapToInt(q -> q.getAnswers().size())
                .sum();
    }

    public long countRightAnswers() {
        return questions.stream()
                .map(Question::getAnswers)
                .flatMap(Collection::stream)
                .filter(Answer::isCorrectly)
                .count();
    }

    public long countQuestionWithoutAnswers() {
        return questions.stream()
                .filter(q -> q.getAnswers().size() == 0)
                .count();
    }

    public long countActiveQuestions() {
        return questions.stream()
                .filter(Question::isActiveQuestion)
                .count();
    }

    public boolean isActiveModule() {
        return countQuestionWithoutAnswers() == 0 && questions.size() > 0 && countActiveQuestions() == questions.size();
    }

    public List<Answer> getAllAnswers() {
        return questions.stream()
                .map(Question::getAnswers)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public List<Answer> getAllRightAnswers() {
        return questions.stream()
                .map(Question::getAnswers)
                .flatMap(Collection::stream)
                .filter(Answer::isCorrectly)
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Module module = (Module) o;

        if (!id.equals(module.id)) return false;
        return title.equals(module.title);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        return result;
    }
}
