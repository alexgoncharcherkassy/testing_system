package com.geekhub.testing_system.admin.module;

import com.geekhub.testing_system.admin.question.Question;
import com.geekhub.testing_system.admin.question.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ModuleServiceImpl implements ModuleService {
    private final ModuleRepository moduleRepository;
    private final QuestionService questionService;

    @Autowired
    public ModuleServiceImpl(ModuleRepository moduleRepository, QuestionService questionService) {
        this.moduleRepository = moduleRepository;
        this.questionService = questionService;
    }

    @Override
    public Module save(Module module) {
        return module.isNew() ? moduleRepository.create(module) : moduleRepository.edit(module);
    }

    @Override
    public Optional<Module> findById(int id) {
        Optional<Module> optionalModule = moduleRepository.findById(id);
        if (optionalModule.isPresent()) {
            List<Module> modules = assignQuestionsToModule(Collections.singletonList(optionalModule.get()));

            return modules.size() == 1 ? Optional.of(modules.get(0)) : Optional.empty();
        }

        return Optional.empty();
    }

    @Override
    public List<Module> findAll() {
        return assignQuestionsToModule(moduleRepository.findAll());
    }

    @Override
    public List<Module> findModulesByIds(Collection<Integer> moduleIds) {
        return moduleRepository.findAllModulesIds(moduleIds);
    }

    private List<Module> assignQuestionsToModule(List<Module> modules) {
        if (modules.size() == 0) {
            return modules;
        }

        Collection<Integer> moduleIds = modules.stream()
                .map(Module::getId)
                .collect(Collectors.toList());

        Map<Integer, Collection<Question>> allQuestionsByModuleIds = questionService.findAllQuestionsByModuleIds(moduleIds);

        for (Module module : modules) {
            if (allQuestionsByModuleIds.containsKey(module.getId())) {
                module.setQuestions(allQuestionsByModuleIds.get(module.getId()));
            }
        }

        return modules;
    }

    @Override
    public Map<Integer, Module> findAllModulesByUserModuleIds(Collection<Integer> userModuleIds) {
        Map<Integer, Module> allModulesByUserModuleIds = moduleRepository.findAllModulesByUserModuleIds(userModuleIds);
        List<Module> modules = new ArrayList<>();

        for (Map.Entry<Integer, Module> entry : allModulesByUserModuleIds.entrySet()) {
            modules.add(entry.getValue());
        }

        List<Module> modulesWithQuestions = assignQuestionsToModule(modules);

        for (Map.Entry<Integer, Module> entry : allModulesByUserModuleIds.entrySet()) {
            for (Module module : modulesWithQuestions) {
                if (entry.getValue().equals(module)) {
                    allModulesByUserModuleIds.replace(entry.getKey(), entry.getValue(), module);
                }
            }
        }

        return allModulesByUserModuleIds;
    }
}
