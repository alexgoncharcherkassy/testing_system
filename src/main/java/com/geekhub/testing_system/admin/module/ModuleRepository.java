package com.geekhub.testing_system.admin.module;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ModuleRepository {
    Module create(Module module);

    Module edit(Module module);

    Optional<Module> findById(int id);

    List<Module> findAll();

    Map<Integer, Module> findAllModulesByUserModuleIds(Collection<Integer> userModuleIds);

    List<Module> findAllModulesIds(Collection<Integer> moduleIds);
}
