package com.geekhub.testing_system.admin.answer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class AnswerServiceImpl implements AnswerService {
    private final AnswerRepository answerRepository;

    @Autowired
    public AnswerServiceImpl(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @Override
    public Answer save(Answer answer, int questionId) {
        return answer.isNew() ? answerRepository.create(answer, questionId) : answerRepository.edit(answer);
    }

    @Override
    public Optional<Answer> findById(int id) {
        return answerRepository.findById(id);
    }

    @Override
    public boolean delete(int id) {
        return answerRepository.delete(id);
    }

    @Override
    public List<Answer> findQuestionsByModule(int id) {
        return answerRepository.findAnswersByQuestion(id);
    }

    @Override
    public Map<Integer, Collection<Answer>> findAllAnswersByQuestionIds(Collection<Integer> questionIds) {
        return answerRepository.findAllAnswersByQuestionIds(questionIds);
    }
}
