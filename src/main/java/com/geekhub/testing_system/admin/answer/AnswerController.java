package com.geekhub.testing_system.admin.answer;

import com.geekhub.testing_system.exception.ResourceValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/admin/questions/{id}/answers")
public class AnswerController {
    private final AnswerService answerService;

    @Autowired
    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @PostMapping("")
    public Answer create(@PathVariable Integer id, @Valid Answer answer, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResourceValidationException("Answer", result.getFieldError().getField(), result.getFieldError().getDefaultMessage());
        }

        return answerService.save(answer, id);
    }

    @GetMapping("")
    public List<Answer> allAnswers(@PathVariable Integer id) {
        return answerService.findQuestionsByModule(id);
    }
}
