package com.geekhub.testing_system.admin.answer;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface AnswerRepository {
    Answer create(Answer answer, int questionId);

    Answer edit(Answer answer);

    Optional<Answer> findById(int id);

    boolean delete(int id);

    List<Answer> findAnswersByQuestion(int id);

    Map<Integer, Collection<Answer>> findAllAnswersByQuestionIds(Collection<Integer> questionIds);
}
