package com.geekhub.testing_system.admin.answer;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Answer {
    private Integer id;

    private Integer questionId;

    @NotNull
    @NotBlank(message = "Title could not be empty")
    @Length(max = 2500)
    private String title;

    private boolean correctly;

    public Answer() {
        this.setCorrectly(false);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCorrectly() {
        return correctly;
    }

    public void setCorrectly(boolean correctly) {
        this.correctly = correctly;
    }

    public boolean isNew() {
        return id == null;
    }

    public boolean checkIsCorrect(boolean result) {
        return correctly == result;
    }
}
