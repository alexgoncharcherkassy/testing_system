package com.geekhub.testing_system.admin.answer;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface AnswerService {
    Answer save(Answer answer, int questionId);

    Optional<Answer> findById(int id);

    boolean delete(int id);

    List<Answer> findQuestionsByModule(int id);

    Map<Integer, Collection<Answer>> findAllAnswersByQuestionIds(Collection<Integer> questionIds);
}
