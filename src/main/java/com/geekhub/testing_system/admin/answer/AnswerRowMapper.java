package com.geekhub.testing_system.admin.answer;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AnswerRowMapper implements RowMapper<Answer> {
    @Override
    public Answer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Answer answer = new Answer();
        answer.setCorrectly(rs.getBoolean("correctly"));
        answer.setId(rs.getInt("id"));
        answer.setQuestionId(rs.getInt("question_id"));
        answer.setTitle(rs.getString("title"));

        return answer;
    }
}
