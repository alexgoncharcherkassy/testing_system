package com.geekhub.testing_system.admin.answer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

@Repository
public class AnswerRepositoryImpl implements AnswerRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AnswerRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Answer create(Answer answer, int questionId) {
        String sql = "INSERT INTO public.answer (title, question_id, correctly) VALUES (?, ?, ?);";

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(c -> {
            PreparedStatement preparedStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, answer.getTitle());
            preparedStatement.setInt(2, questionId);
            preparedStatement.setBoolean(3, answer.isCorrectly());

            return preparedStatement;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();

        for (Map.Entry<String, Object> entry : keys.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("id")) {
                answer.setId((Integer) entry.getValue());
            }
        }

        return answer;
    }

    @Override
    public Answer edit(Answer answer) {
        String sql = "UPDATE public.answer SET title = ?, correctly = ? WHERE id = ?;";

        try {
            jdbcTemplate.update(sql, answer.getTitle(), answer.isCorrectly(), answer.getId());
        } catch (DataAccessException e) {
        }

        return answer;
    }

    @Override
    public Optional<Answer> findById(int id) {
        String sql = "SELECT id, title, question_id, correctly FROM public.answer WHERE id = ?;";

        try {
            Answer answer = jdbcTemplate.queryForObject(sql, new Object[]{id}, new AnswerRowMapper());

            return Optional.of(answer);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean delete(int id) {
        String sql = "DELETE FROM public.answer WHERE id = ?;";

        int updateRows = jdbcTemplate.update(sql, id);

        return updateRows > 0;
    }

    @Override
    public List<Answer> findAnswersByQuestion(int id) {
        String sql = "SELECT id, title, question_id, correctly FROM public.answer WHERE question_id = ?;";

        return jdbcTemplate.query(sql, new Object[]{id}, new AnswerRowMapper());
    }

    @Override
    public Map<Integer, Collection<Answer>> findAllAnswersByQuestionIds(Collection<Integer> questionIds) {
        if (questionIds.size() == 0) {
            return new HashMap<>();
        }

        String sql = "SELECT id, title, question_id, correctly FROM public.answer WHERE question_id IN (:ids);";

        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", questionIds);

        return template.query(sql, parameters, (ResultSet rs) -> {
            Map<Integer, Collection<Answer>> answersByQuestionId = new HashMap<>();

            while (rs.next()) {
                Answer answer = new Answer();
                answer.setId(rs.getInt("id"));
                answer.setTitle(rs.getString("title"));
                answer.setCorrectly(rs.getBoolean("correctly"));
                answer.setQuestionId(rs.getInt("question_id"));

                answersByQuestionId.computeIfPresent(answer.getQuestionId(), (k, v) -> {
                    v.add(answer);
                    return v;
                });
                answersByQuestionId.computeIfAbsent(answer.getQuestionId(), k -> new ArrayList<>(Collections.singletonList(answer)));
            }

            return answersByQuestionId;
        });
    }
}
