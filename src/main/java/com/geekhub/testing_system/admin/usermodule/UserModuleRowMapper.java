package com.geekhub.testing_system.admin.usermodule;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserModuleRowMapper implements RowMapper<UserModule> {
    @Override
    public UserModule mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserModule userModule = new UserModule();
        userModule.setId(rs.getInt("id"));
        userModule.setUserId(rs.getInt("user_id"));
        userModule.setStatus(ModuleStatus.valueOf(rs.getString("status")));
        userModule.setRating(rs.getFloat("rating"));
        userModule.setCreatedAt(rs.getTimestamp("created_at").toLocalDateTime());
        userModule.setFinishedAt(rs.getTimestamp("finished_at") != null ? rs.getTimestamp("finished_at").toLocalDateTime() : null);

        return userModule;
    }
}
