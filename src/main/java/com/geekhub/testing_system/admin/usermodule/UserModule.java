package com.geekhub.testing_system.admin.usermodule;

import com.geekhub.testing_system.admin.module.Module;
import com.geekhub.testing_system.user.User;

import java.time.LocalDateTime;

public class UserModule {
    private Integer id;
    private Integer userId;
    private Module module;
    private ModuleStatus status;
    private Float rating;
    private LocalDateTime createdAt;
    private LocalDateTime finishedAt;

    public UserModule() {
        setStatus(ModuleStatus.IN_PROGRESS);
        setRating(0.0F);
        setCreatedAt(LocalDateTime.now());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public ModuleStatus getStatus() {
        return status;
    }

    public void setStatus(ModuleStatus status) {
        this.status = status;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(LocalDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public float getRatingPercent() {
        return (rating * 100) / module.getRating();
    }
}
