package com.geekhub.testing_system.admin.usermodule;

import java.util.ArrayList;
import java.util.List;

public class UserModuleResponse {
    private List<UserModule> userModules = new ArrayList<>();

    public List<UserModule> getUserModules() {
        return new ArrayList<>(userModules);
    }

    public void setUserModules(List<UserModule> userModules) {
        this.userModules = userModules;
    }

    public void addUserModule(UserModule userModule) {
        userModules.add(userModule);
    }
}
