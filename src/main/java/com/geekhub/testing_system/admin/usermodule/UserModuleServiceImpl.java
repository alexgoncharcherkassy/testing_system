package com.geekhub.testing_system.admin.usermodule;

import com.geekhub.testing_system.admin.module.Module;
import com.geekhub.testing_system.admin.module.ModuleService;
import com.geekhub.testing_system.modules.passmodule.PassModule;
import com.geekhub.testing_system.user.User;
import com.geekhub.testing_system.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserModuleServiceImpl implements UserModuleService {
    private final UserModuleRepository userModuleRepository;
    private final ModuleService moduleService;
    private final UserRepository userRepository;

    @Autowired
    public UserModuleServiceImpl(UserModuleRepository userModuleRepository, ModuleService moduleService, UserRepository userRepository) {
        this.userModuleRepository = userModuleRepository;
        this.moduleService = moduleService;
        this.userRepository = userRepository;
    }

    @Override
    public List<UserModule> findUserModuleByUser(User user) {
        List<UserModule> userModules = userModuleRepository.findUserModuleByUserAndModuleStatuses(user, ModuleStatus.getAllStatuses());

        return assignModuleToUserModuleBy(userModules);
    }

    @Override
    public List<UserModule> findCompletedUserModuleByUser(User user) {
        List<UserModule> userModules = userModuleRepository.findUserModuleByUserAndModuleStatuses(user, ModuleStatus.getAllCompletedStatuses());

        return assignModuleToUserModuleBy(userModules);
    }

    @Override
    public List<UserModule> findInProgressUserModuleByUser(User user) {
        List<UserModule> userModules = userModuleRepository.findUserModuleByUserAndModuleStatuses(user, ModuleStatus.getInProgressStatus());

        return assignModuleToUserModuleBy(userModules);
    }

    @Override
    @Transactional
    public UserModuleResponse save(UserModuleRequest request, int userId) {
        UserModuleResponse userModuleResponse = new UserModuleResponse();
        Optional<User> optionalUser = userRepository.findById(userId);

        if (optionalUser.isPresent()) {
            Collection<Integer> moduleIds = request.getModules().stream()
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());

            List<Module> modules = moduleService.findModulesByIds(moduleIds);

            User user = optionalUser.get();

            List<UserModule> userModules = new ArrayList<>();

            for (Module module : modules) {
                UserModule userModule = new UserModule();
                userModule.setUserId(user.getId());
                userModule.setModule(module);

                userModules.add(userModule);
            }

            List<UserModule> createdUserModules = userModuleRepository.create(userModules);

            userModuleResponse.setUserModules(createdUserModules);
        }

        return userModuleResponse;
    }

    @Override
    public Optional<UserModule> findById(int id) {
        Optional<UserModule> optionalUserModule = userModuleRepository.findById(id);

        if (optionalUserModule.isPresent()) {
            List<UserModule> userModules = assignModuleToUserModuleBy(Collections.singletonList(optionalUserModule.get()));

            return userModules.size() > 0 ? Optional.of(userModules.get(0)) : Optional.empty();
        }

        return Optional.empty();
    }

    @Override
    public UserModule update(UserModule userModule) {
        return userModuleRepository.edit(userModule);
    }

    @Override
    public Map<Integer, Collection<UserModule>> findAllUserModulesByUserIds(Collection<Integer> userIds, ModuleStatus status) {
        Map<Integer, Collection<UserModule>> allUserModulesByUserIds = userModuleRepository.findAllUserModulesByUserIds(userIds, status);

        List<Integer> userModuleIds = new ArrayList<>();

        for (Map.Entry<Integer, Collection<UserModule>> entry : allUserModulesByUserIds.entrySet()) {
            userModuleIds.addAll(entry.getValue().stream()
                    .map(UserModule::getId)
                    .collect(Collectors.toList()));
        }

        Map<Integer, Module> allModulesByUserModuleIds = moduleService.findAllModulesByUserModuleIds(userModuleIds);

        for (Map.Entry<Integer, Collection<UserModule>> entry : allUserModulesByUserIds.entrySet()) {
            Collection<UserModule> userModules = entry.getValue();
            for (UserModule userModule : userModules) {
                if (allModulesByUserModuleIds.containsKey(userModule.getId())) {
                    userModule.setModule(allModulesByUserModuleIds.get(userModule.getId()));
                }
            }
        }

        return allUserModulesByUserIds;
    }

    private List<UserModule> assignModuleToUserModuleBy(List<UserModule> userModules) {
        if (userModules.size() == 0) {
            return userModules;
        }

        Collection<Integer> userModuleIds = userModules.stream()
                .map(UserModule::getId)
                .collect(Collectors.toList());

        Map<Integer, Module> allModulesByUserModuleIds = moduleService.findAllModulesByUserModuleIds(userModuleIds);

        for (UserModule userModule : userModules) {
            if (allModulesByUserModuleIds.containsKey(userModule.getId())) {
                userModule.setModule(allModulesByUserModuleIds.get(userModule.getId()));
            }
        }

        return userModules;
    }

    @Override
    public Optional<UserModule> findUserModuleByPassModule(PassModule passModule) {
        Optional<UserModule> optionalUserModule = userModuleRepository.findUserModuleByPassModule(passModule);

        if (optionalUserModule.isPresent()) {
            List<UserModule> userModules = assignModuleToUserModuleBy(Collections.singletonList(optionalUserModule.get()));

            return userModules.size() > 0 ? Optional.of(userModules.get(0)) : Optional.empty();
        }

        return Optional.empty();
    }
}
