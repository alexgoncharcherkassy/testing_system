package com.geekhub.testing_system.admin.usermodule;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum ModuleStatus {
    IN_PROGRESS, SUCCESS, FAILED;

    public static List<String> getAllStatuses() {
        return Arrays.asList(IN_PROGRESS.name(), SUCCESS.name(), FAILED.name());
    }

    public static List<String> getAllCompletedStatuses() {
        return Arrays.asList(SUCCESS.name(), FAILED.name());
    }

    public static List<String> getInProgressStatus() {
        return Collections.singletonList(IN_PROGRESS.name());
    }
}
