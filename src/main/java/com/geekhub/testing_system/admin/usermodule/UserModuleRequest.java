package com.geekhub.testing_system.admin.usermodule;

import java.util.ArrayList;
import java.util.List;

public class UserModuleRequest {
    private List<String> modules = new ArrayList<>();

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }
}
