package com.geekhub.testing_system.admin.usermodule;

import com.geekhub.testing_system.modules.passmodule.PassModule;
import com.geekhub.testing_system.user.User;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UserModuleRepository {
    List<UserModule> findUserModuleByUserAndModuleStatuses(User user, Collection<String> statuses);

    List<UserModule> create(List<UserModule> userModules);

    UserModule edit(UserModule userModule);

    Optional<UserModule> findById(int id);

    Map<Integer, Collection<UserModule>> findAllUserModulesByUserIds(Collection<Integer> userIds, ModuleStatus status);

    Optional<UserModule> findUserModuleByPassModule(PassModule passModule);
}
