package com.geekhub.testing_system.admin.usermodule;

import com.geekhub.testing_system.modules.passmodule.PassModule;
import com.geekhub.testing_system.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository
public class UserModuleRepositoryImpl implements UserModuleRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserModuleRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<UserModule> findUserModuleByUserAndModuleStatuses(User user, Collection<String> statuses) {
        String sql = "SELECT id, user_id, module_id, status, rating, created_at, finished_at FROM user_module WHERE user_id = :user_id AND status IN (:statuses);";

        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("user_id", user.getId());
        parameters.addValue("statuses", statuses);

        return template.query(sql, parameters, new UserModuleRowMapper());
    }

    @Override
    public List<UserModule> create(List<UserModule> userModules) {
        String sql = "INSERT INTO user_module (module_id, user_id, rating, status, created_at) VALUES (?, ?, ?, ?, ?);";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                UserModule userModule = userModules.get(i);
                ps.setInt(1, userModule.getModule().getId());
                ps.setInt(2, userModule.getUserId());
                ps.setFloat(3, userModule.getRating());
                ps.setString(4, userModule.getStatus().toString());
                ps.setTimestamp(5, Timestamp.valueOf(userModule.getCreatedAt()));
            }

            @Override
            public int getBatchSize() {
                return userModules.size();
            }
        });

        return userModules;
    }

    @Override
    public Optional<UserModule> findById(int id) {
        String sql = "SELECT id, user_id, module_id, status, rating, created_at, finished_at FROM user_module WHERE id = ?;";

        try {
            UserModule userModule = jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserModuleRowMapper());
            return Optional.of(userModule);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public UserModule edit(UserModule userModule) {
        String sql = "UPDATE user_module SET status = ?, rating = ?, finished_at = ? WHERE id = ?;";

        try {
            jdbcTemplate.update(sql, userModule.getStatus().toString(), userModule.getRating(), Timestamp.valueOf(userModule.getFinishedAt()), userModule.getId());
            return userModule;
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public Map<Integer, Collection<UserModule>> findAllUserModulesByUserIds(Collection<Integer> userIds, ModuleStatus status) {
        if (userIds.size() == 0) {
            return new HashMap<>();
        }

        String sql;

        if (status == null) {
            sql = "SELECT id, user_id, module_id, status, rating, created_at, finished_at FROM user_module WHERE user_id IN (:ids);";
        } else {
            sql = "SELECT id, user_id, module_id, status, rating, created_at, finished_at FROM user_module WHERE user_id IN (:ids) AND status = (:status);";
        }

        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", userIds);

        if (status != null) {
            parameters.addValue("status", status.name());
        }

        return template.query(sql, parameters, (ResultSet rs) -> {
            Map<Integer, Collection<UserModule>> userModulesByModuleId = new HashMap<>();

            while (rs.next()) {
                UserModule userModule = new UserModule();
                userModule.setId(rs.getInt("id"));
                userModule.setFinishedAt(rs.getTimestamp("finished_at") != null ? rs.getTimestamp("finished_at").toLocalDateTime() : null);
                userModule.setCreatedAt(rs.getTimestamp("created_at").toLocalDateTime());
                userModule.setStatus(ModuleStatus.valueOf(rs.getString("status")));
                userModule.setRating(rs.getFloat("rating"));
                userModule.setUserId(rs.getInt("user_id"));

                userModulesByModuleId.computeIfPresent(userModule.getUserId(), (k, v) -> {
                    v.add(userModule);
                    return v;
                });
                userModulesByModuleId.computeIfAbsent(userModule.getUserId(), k -> new ArrayList<>(Collections.singletonList(userModule)));

            }

            return userModulesByModuleId;
        });
    }

    @Override
    public Optional<UserModule> findUserModuleByPassModule(PassModule passModule) {
        String sql = "SELECT um.id, um.user_id, um.module_id, um.status, um.rating, um.created_at, um.finished_at FROM user_module um " +
                "JOIN pass_module pm ON um.id = pm.user_module_id WHERE pm.id = ?;";

        try {
            UserModule userModule = jdbcTemplate.queryForObject(sql, new Object[]{passModule.getId()}, new UserModuleRowMapper());
            return Optional.of(userModule);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }
}
