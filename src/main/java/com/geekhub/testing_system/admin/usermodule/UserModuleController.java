package com.geekhub.testing_system.admin.usermodule;

import com.geekhub.testing_system.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/users/{id}/modules")
public class UserModuleController {
    private final UserModuleService userModuleService;

    @Autowired
    public UserModuleController(UserModuleService userModuleService) {
        this.userModuleService = userModuleService;
    }

    @PostMapping("")
    public UserModuleResponse saveUserModule(@PathVariable Integer id, UserModuleRequest userModuleRequest) {
        UserModuleResponse userModuleResponse = userModuleService.save(userModuleRequest, id);

        if (userModuleResponse.getUserModules().size() == 0) {
            throw new ResourceNotFoundException("User", "id", id);
        }

        return userModuleResponse;
    }
}
