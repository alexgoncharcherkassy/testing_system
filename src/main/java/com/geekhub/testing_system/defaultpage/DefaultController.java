package com.geekhub.testing_system.defaultpage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DefaultController {
    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }
}
