ALTER TABLE public."user" ALTER COLUMN created_at DROP DEFAULT;
ALTER TABLE public.user_module ALTER COLUMN created_at DROP DEFAULT;
ALTER TABLE public.pass_module ALTER COLUMN time_start DROP DEFAULT;
ALTER TABLE public.module ALTER COLUMN created_at DROP DEFAULT;