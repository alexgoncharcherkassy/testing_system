CREATE TABLE public.question
(
  id SERIAL PRIMARY KEY NOT NULL,
  title TEXT NOT NULL,
  module_id INT NOT NULL,
  FOREIGN KEY (module_id) REFERENCES module (id) ON DELETE CASCADE
);