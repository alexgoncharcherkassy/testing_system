CREATE TABLE public.answer
(
  id SERIAL PRIMARY KEY NOT NULL,
  title TEXT NOT NULL,
  correctly BOOLEAN NOT NULL,
  question_id INT NOT NULL,
  FOREIGN KEY (question_id) REFERENCES question (id) ON DELETE CASCADE
);