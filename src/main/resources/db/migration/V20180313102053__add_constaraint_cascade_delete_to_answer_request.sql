ALTER TABLE public.answer_request DROP CONSTRAINT answer_request_pass_module_id_fkey;
ALTER TABLE public.answer_request
  ADD CONSTRAINT answer_request_pass_module_id_fkey
FOREIGN KEY (pass_module_id) REFERENCES pass_module (id) ON DELETE CASCADE;