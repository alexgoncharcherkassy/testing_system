CREATE TABLE public.user_module
(
  id SERIAL PRIMARY KEY NOT NULL,
  user_id INT NOT NULL REFERENCES public.user (id),
  module_id INT NOT NULL REFERENCES public.module (id),
  status VARCHAR(50) NOT NULL,
  rating FLOAT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now()
);