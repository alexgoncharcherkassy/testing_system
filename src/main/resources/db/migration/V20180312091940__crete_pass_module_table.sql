CREATE TABLE public.pass_module
(
  id SERIAL PRIMARY KEY NOT NULL,
  rating FLOAT DEFAULT 0 NOT NULL,
  status VARCHAR(50) NOT NULL,
  time_start TIMESTAMP DEFAULT now() NOT NULL,
  time_end TIMESTAMP,
  user_module_id INT NOT NULL,
  FOREIGN KEY (user_module_id) REFERENCES user_module (id) ON DELETE CASCADE
);