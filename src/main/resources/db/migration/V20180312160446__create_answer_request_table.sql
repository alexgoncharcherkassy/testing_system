CREATE TABLE public.answer_request
(
  id SERIAL PRIMARY KEY NOT NULL,
  pass_module_id INT NOT NULL,
  answer_id INT NOT NULL,
  result BOOLEAN NOT NULL,
  FOREIGN KEY (pass_module_id) REFERENCES pass_module (id)
);