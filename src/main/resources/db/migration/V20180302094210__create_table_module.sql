CREATE TABLE public.module
(
    id SERIAL PRIMARY KEY NOT NULL,
    title VARCHAR(100) NOT NULL,
    rating INT NOT NULL,
    percent_success FLOAT NOT NULL,
    attempts INT NOT NULL,
    time INT NOT NULL,
    created_at TIMESTAMP DEFAULT now() NOT NULL
);