$('.saveUserModules').click(function (e) {
    e.preventDefault();
    var form = $(this).closest('.modal-content').find('form');
    jQuery.ajax({
        method: 'POST',
        url: form.prop('action'),
        data: form.serialize()
    })
        .done(function (resp) {
            $('.error').remove();
            form.closest('.modal').modal('hide');
            $.each(resp.userModules, function (key, value) {
                form.closest('tr').find('.assignedModules').append($('<span>').css('display', 'block').text(value.module.title));
                form.find('select option').each(function () {
                    if (parseInt($(this).val()) === parseInt(value.module.id)) {
                        $(this).remove();
                    }
                })
            });
        })
        .fail(function (resp) {
            $('.error').remove();
            var mesError = resp.responseJSON.message;
            form.find('select').after($('<span>').addClass('error').text(mesError));
        })
})