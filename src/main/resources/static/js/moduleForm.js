$(".answersDiv").hide();

var counter = 1;

$('#addQuestion').click(function () {
    $('#modalQuestion').find('.modal-body > form').trigger('reset');
    $('#modalQuestion').find('.modal-body > .currentQuestions').remove();
    $("#addAnswer").prop('disabled', true);
    $("#saveQuestion").prop('disabled', false);

    counter = 1;
});

var formQuestion = $("form[name='question']");
var formAnswer = $("form[name='answer']");

$('#saveQuestion').click(function (e) {
    e.preventDefault();
    jQuery.ajax({
        method: 'POST',
        url: formQuestion.prop('action'),
        data: formQuestion.serialize()
    })
        .done(function (resp) {
            $('.error').remove();
            $("#addAnswer").prop('disabled', false);
            $("#saveQuestion").prop('disabled', true);
            formAnswer.prop('action', '/admin/questions/' + resp.id + '/answers');
            $('.questionGroup')
                .append($('<li>').addClass('list-group-item question').attr('data-id', resp.id)
                    .append($('<span>').text(resp.title)
                    )
                    .append($('<div>').addClass('answersDiv col-lg-12').hide()
                        .append($('<span>').text('Answers'))
                        .append($('<ul>').addClass('list-group'))
                    )
                );
            $(".question[data-id=" + resp.id + "]").click(clickOnQuestion);
        })
        .fail(function (resp) {
            $('.error').remove();
            var mesError = resp.responseJSON.message;
            formQuestion.find('textarea').after($('<span>').addClass('error').text(mesError));
        })
});

$('#saveAnswer').click(function (e) {
    e.preventDefault();
    jQuery.ajax({
        method: 'POST',
        url: formAnswer.prop('action'),
        data: formAnswer.serialize()
    })
        .done(function (resp) {
            $('.error').remove();
            $('#modalAnswer').modal('hide');
            formAnswer.trigger("reset");
            $('#modalQuestion').find('.modal-body')
                .append($('<div>').addClass('currentQuestions')
                    .append($('<span>').text('Answer ' + counter++ + " : " + resp.title)
                    )
                );
        })
        .fail(function (resp) {
            $('.error').remove();
            var mesError = resp.responseJSON.message;
            formAnswer.find('textarea').after($('<span>').addClass('error').text(mesError));
        })
});

$(".question").click(clickOnQuestion);

function clickOnQuestion() {
    var id = $(this).attr('data-id');
    var _this = $(this);
    if ($(this).attr('data-open')) {
        $(this).removeAttr('data-open');
        $(this).find('.answersDiv').hide();
    } else {
        $(this).attr('data-open', true);
        $(this).find('.answersDiv').show();
        jQuery.ajax({
            method: 'GET',
            url: "/admin/questions/" + id + "/answers"
        })
            .done(function (resp) {
                _this.find('.answersDiv > ul').empty();

                $.each(resp, function (key, value) {
                    var textAnswer = 'Title: ' + value.title + ' ------ Correctly: ' + (value.correctly ? 'Yes' : 'No');
                    _this.find('.answersDiv > ul')
                        .append($('<li>').addClass('list-group-item answerItem')
                            .append($('<span>').text(textAnswer)
                            )
                        );
                })
            })
    }
}