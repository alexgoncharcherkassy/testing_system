$('#questionDiv > div').first().addClass('active').show();
$('#next').click(function (e) {
    e.preventDefault();
    if ($('.active').next().length > 0) {
        $('.active').removeClass('active').hide().next().addClass('active').show();
    } else {
        $('#next').hide();
        $('#finish').show();
    }
});
$('#prev').click(function (e) {
    e.preventDefault();
    if ($('.active').prev().length > 0) {
        $('#next').show();
        $('#finish').hide();
        $('.active').removeClass('active').hide().prev().addClass('active').show();
    }
});

var countDownDate = new Date(expired).getTime();

if ($("#timeLeft").length > 0) {
    var x = setInterval(function () {

        var now = new Date().getTime();

        var distance = countDownDate - now;

        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        $("#timeLeft").text("Time left: " + hours + "h " + minutes + "m " + seconds + "s ");

        if (distance < 0) {
            clearInterval(x);
            $("#timeLeft").text("EXPIRED").css("color", "red");
            $('#prev').hide();
            $('#next').hide();
            $('#finish').show();
        }
    }, 1000);
}

$("form[name='sbtResult']").submit(function (e) {
    e.preventDefault();
    var _this = $(this);
    jQuery.ajax({
        method: 'POST',
        url: _this.prop('action'),
        data: _this.serialize()
    })
        .done(function (resp) {
            location.reload();
        })
        .fail(function (resp) {
            alert('Something went wrong. Try again')
        });

    return false;
});

$('input[type="checkbox"]').change(function (e) {
    var form = $(this).closest('form');
    jQuery.ajax({
        method: 'POST',
        url: form.prop('action'),
        data: form.serialize()
    })
        .done(function (resp) {
        })
        .fail(function (resp) {
            alert("Something went wrong. Try again");
        })

});