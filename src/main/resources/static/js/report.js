$('.module').change(function (e) {
    e.preventDefault();
    window.location.href = window.location.href.replace(/[\?#].*|$/, "?module=" + $(this).val());
});

$('.module').find('option').each(function () {
    if ($(this).val() === moduleId) {
        $(this).prop('selected', true);
    } else {
        $(this).prop('selected', false);
    }
})